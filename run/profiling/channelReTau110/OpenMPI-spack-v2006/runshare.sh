#!/bin/bash

# LANG
export LANG=C
export LC_ALL=C

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Purge module
module purge

# OpenFOAM
spack load openfoam@2006

# Tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Application name
application=$(getApplication)

# MPI run command
mpirun="mpiexec --mca plm_rsh_agent /bin/pjrsh -machinefile $PJM_O_NODEINF"
#mpirun="mpiexec --mca plm_rsh_agent /bin/pjrsh -machinefile $PJM_O_NODEINF --mca btl openib,vader,self"
