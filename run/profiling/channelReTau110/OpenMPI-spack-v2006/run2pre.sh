#!/bin/bash
#PJM -L "rscgrp=ito-ss-dbg"
#PJM -L "rscunit=ito-a"
#PJM -L "vnode=1"
#PJM -L "vnode-core=36"
#PJM -L "elapse=1:00:00"
#PJM --mpi "proc=36"
#PJM -S

# Share settings
. ./runshare.sh

# Run command
decomposePar -cellDist >& log.decomposePar.$jobid
