set terminal pdf color solid font "Times,18"
set xlabel "Iterations"
set ylabel "Average execution time error [%]"
set nokey
set output "executionTimeError.pdf"
set style data line
tave=(t1-t0)/(n-1)
set grid ytics mytics xtics mxtics
set xrange [ x0:x1 ]
set yrange [ y0:y1 ]
plot sprintf('< grep "^ExecutionTime" %s',file) using ($0+1):(((($3-t0)/$0)-tave)/tave*100.0)
