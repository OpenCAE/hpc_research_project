#!/bin/bash
#PJM -L "rscgrp=ito-s-dbg"
#PJM -L "rscunit=ito-a"
#PJM -L "vnode=2"
#PJM -L "vnode-core=36"
#PJM -L "elapse=1:00:00"
#PJM --mpi "proc=72"
#PJM -S

# Share settings
. ./runshare.sh

# Profiling using Intel MPI
# Profile all MPI rank (software sampling)
export I_MPI_GTOOL="amplxe-cl -collect hotspots -knob sampling-mode=sw -r vtune.$jobid:all=node-wide"
# Profile all MPI rank (hardware sampling)
#export I_MPI_GTOOL="amplxe-cl -collect hotspots -knob sampling-mode=hw -r vtune.$jobid:all=node-wide"
# Profile MPI rank 0
#export I_MPI_GTOOL="amplxe-cl -collect hotspots -knob sampling-mode=sw -r vtune.$jobid:0"

# Print environment variables
env

# Run command
$mpirun -np $PJM_MPI_PROC $application -parallel >& log.$application.$jobid
