#!/bin/bash

# LANG
export LANG=C
export LC_ALL=C

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Load Intel MPI module
module purge
module load intel/2019.4
export MPI_ROOT=$I_MPI_ROOT

# OpenFOAM
. $HOME/OpenFOAM/OpenFOAM-v1912/etc/bashrc \
WM_COMPILER_TYPE=system \
WM_COMPILER=Gcc4_8_5 \
WM_COMPILE_OPTION=Opt \
WM_ARCH_OPTION=64 \
WM_PRECISION_OPTION=DP \
WM_LABEL_SIZE=32 \
WM_MPLIB=INTELMPI2019_4_243

# Tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Application name
application=$(getApplication)

# Intel MPI settings
unset I_MPI_PIN_DOMAIN
export I_MPI_HYDRA_BOOTSTRAP=rsh
export I_MPI_HYDRA_BOOTSTRAP_EXEC=/bin/pjrsh
export I_MPI_HYDRA_HOST_FILE=$PJM_O_NODEINF
export I_MPI_PERHOST=36 

# MPI run command
mpirun="mpiexec.hydra"

