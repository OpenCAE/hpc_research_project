#!/bin/bash

# LANG
export LANG=C
export LC_ALL=C

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Load OpenMPI module
module purge
module load openmpi/3.1.1-nocuda-gcc4.8.5

# OpenFOAM
. $HOME/OpenFOAM/OpenFOAM-v1912/etc/bashrc \
WM_COMPILER_TYPE=system \
WM_COMPILER=Gcc4_8_5 \
WM_COMPILE_OPTION=Opt \
WM_ARCH_OPTION=64 \
WM_PRECISION_OPTION=DP \
WM_LABEL_SIZE=32 \
WM_MPLIB=SYSTEMOPENMPI3_1_1

# Tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Application name
application=$(getApplication)

# MPI run command
mpirun="mpiexec --mca plm_rsh_agent /bin/pjrsh -machinefile $PJM_O_NODEINF --map-by core"
