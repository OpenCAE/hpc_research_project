#!/bin/bash
#PJM -L "rscgrp=ito-s-dbg"
#PJM -L "rscunit=ito-a"
#PJM -L "vnode=2"
#PJM -L "vnode-core=36"
#PJM -L "elapse=1:00:00"
#PJM --mpi "proc=72"
#PJM -S

# Share settings
. ./runshare.sh

# Print environment variables
env

# Run command
/home/app/intel/intel2019_up4/vtune_amplifier_2019.4.0.597835/bin64/amplxe-cl \
-collect hotspots -knob sampling-mode=sw -r vtune.$jobid -- \
$mpirun -np $PJM_MPI_PROC $application -parallel >& log.$application.$jobid
