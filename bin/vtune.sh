#!/bin/sh

# VTune
module load intel/2019.4

# Python for gprof2dot
module load python/3.6.2

for dir in vtune.*/
do
    r=${dir%/}
    echo $r
    [ -d $r/archive ] || exit 0
    for report_type in \
    callstacks \
    hotspots \
    summary \
    top-down
    do
    log=$r.$report_type.csv
    [ -f $log ] || amplxe-cl -R $report_type -r $r -format=csv -csv-delimiter=',' > $log
    done

    report_type=gprof-cc
    log=$r.$report_type.txt
    [ -f $log ] || amplxe-cl -R $report_type -r $r > $log
    if command -v gprof2dot >/dev/null && command -v dot >/dev/null
    then
	pdf=$r.$report_type.pdf
	[ -f $pdf ] || gprof2dot -f axe $log | dot -T pdf -o $pdf
    fi
done
