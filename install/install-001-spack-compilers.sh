#!/bin/sh
# Find system compilers
for module in \
gcc/9.2.0 \
intel/2016 \
intel/2017 \
intel/2018 \
intel/2018.3 \
intel/2019.4
do
  module purge
  module load $module
  spack compiler find
done

# Install gcc@10.1.0
module purge
spack compiler list | grep gcc@10.1.0 >& /dev/null || spack install gcc@10.1.0

# Update compiler list
spack compiler find
spack compiler list
