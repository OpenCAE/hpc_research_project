#!/bin/sh
cd
git clone https://github.com/spack/spack.git
export SPACK_ROOT=$HOME/spack
. $SPACK_ROOT/share/spack/setup-env.sh
export LC_ALL=ja_JP.utf8
